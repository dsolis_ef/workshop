<?php

namespace Acme\HelloBundle\Controller;
 
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

 
class HelloController extends Controller
{
	public function indexAction($name)
    {
        //return new Response('<html><body>Hello '.$name.'!!!! Aguaguaguanta!!!</body></html>');
        //throw new \Exception('Algo no ha salido bien.');

        return $this->render('AcmeHelloBundle:Hello:index.html.twig', array('name' => $name));
    }
}