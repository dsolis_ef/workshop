<?php

namespace DSG\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use DSG\UserBundle\Entity\User;
use DSG\UserBundle\Form\UserType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError;


class UserController extends Controller
{
    public function homeAction(){
        return $this->render('DSGUserBundle:User:home.html.twig');

    }
    public function indexAction(Request $request)
    {
        //return $this->render('DSGUserBundle:Default:index.html.twig', array('name' => $name));
       //return  new Response('Modulo de usuarios');

    	$em=$this->getDoctrine()->getManager();

    	// $users=$em->getRepository('DSGUserBundle:User')->findAll();





    	/*$res="Lista de usuarios <br>";

    	foreach ($users as $user) {
    		$res.='Usuario' .$user->getUsername(). '- Email:'. $user->getEmail() . '<br />';
    	}

    	return new Response($res);*/

    	$dql="Select u from DSGUserBundle:User u order by u.id DESC";

    	$users= $em->createQuery($dql);

    	$paginator = $this->get('knp_paginator');

    	$pagination = $paginator->paginate(

    		$users, $request->query->getInt('page',1),5
    	);

    	$deleteFormAjax = $this->createCustomForm(':USER_ID','DELETE','dsg_user_delete');

    	//return $this->render('DSGUserBundle:User:index.html.twig', array('users' => $users));
    	return $this->render('DSGUserBundle:User:index.html.twig', array('pagination' => $pagination, 'delete_form_ajax' => $deleteFormAjax->createView() ));
    }

    public function addAction(){

    	$user = new User();
    	$form = $this->createCreateForm($user);


    	return $this->render('DSGUserBundle:User:add.html.twig',array('form'=> $form->createView()));



    }

    private function createCreateForm(User $entity){

    	$form =$this->createForm(new UserType(), $entity, array(
    		'action'=> $this->generateUrl('dsg_user_create'),
    		'method'=>'POST'
    		));

    	return $form;


    }

    public function createAction(Request $request){

    	$user=new User();

    	$form = $this->createCreateForm($user);
    	$form->handleRequest($request);

    	if($form->isValid()){

    		$password=$form->get('password')->getData();

    		$passwordConstraint = new Assert\NotBlank();
    		$errorList = $this->get('validator')->validate($password, $passwordConstraint);

    		if(count($errorList)==0){

    		$encoder = $this->container->get('security.password_encoder');

    		$encoded = $encoder->encodePassword($user,$password);

    		$user->setPassword($encoded);


    		$em = $this->getDoctrine()->getManager();
    		$em->persist($user);
    		$em->flush();

    		$successMessage = $this->get('translator')->trans('The user has been created.');


    		$this->addFlash('mensaje',$successMessage);

    		return $this->redirectToRoute('dsg_user_index');
    	}
    	else
    	{
    				$errorMessage=new FormError($errorList[0]->getMessage());
    				$form->get('password')->addError($errorMessage);

    	}

    	}


    	return $this->render('DSGUserBundle:User:add.html.twig',array('form'=> $form->createView()));
 




    }

    public function editAction($id){

    	$em=$this->getDoctrine()->getManager();

    	$user = $em->getRepository('DSGUserBundle:User')->find($id);

    	if(!$user){ //a pagina 404

    		$msgexc = $this->get('translator')->trans('User not found');

    		throw $this->createNotFoundException($msgexc);
    	}
    	$form = $this->createEditForm($user);

    	return $this->render('DSGUserBundle:User:edit.html.twig', array('user'=>$user,'form'=> $form->createView()));


    }

    private function createEditForm(User $entity){
    	$form=$this->createForm(new UserType(), $entity, array('action' => $this->generateUrl('dsg_user_update', array('id' => $entity->getId()) ), 'method' => 'PUT'));

    	return $form;

    }

    public function updateAction($id, Request $request){

    	$em=$this->getDoctrine()->getManager();

    	$user = $em->getRepository('DSGUserBundle:User')->find($id);

    	if(!$user){ //a pagina 404

    		$msgexc = $this->get('translator')->trans('User not found');

    		throw $this->createNotFoundException($msgexc);
    	}

    	$form = $this->createEditForm($user);

    	$form->handleRequest($request);

    	if($form->isSubmitted() && $form->isValid()){

    		$password = $form->get('password')->getData();

    		if(!empty($password))
    		{

    			$encoder = $this->container->get('security.password_encoder');
                
    			$encoded = $encoder->encodePassword($user,$password);
                

    			$user->setPassword($encoded);

    		}
    		else{

    			$recoverPass=$this->recoverPass($id);
    			$user->setPassword($recoverPass[0]['password']);

    		}

    		if($form->get('role')->getData() == 'ROLE_ADMIN')
    		{
    			$user->setIsActive(1);

    		}	


    		$em->flush();

    		$successMessage=$this->get('translator')->trans('The user has been modified');

    		$this->addFlash('mensaje',$successMessage);

    		return $this->redirectToRoute('dsg_user_edit',array('id'=> $user->getId()));
    	}


    	return $this->render('DSGUserBundle:User:edit.html.twig', array('user'=>$user, 'form' => $form->createView()));



    }

    private function recoverPass($id){
    	$em=$this->getDoctrine()->getManager();

    	$query = $em->createQuery(
    			'select u.password from DSGUserBundle:User u where u.id=:id'
    		)->setParameter('id', $id);

    	$currentPass = $query->getResult();

    	return $currentPass;

    }

    public function viewAction($id){
    	$repository=$this->getDoctrine()->getRepository("DSGUserBundle:User");

    	$user=$repository->find($id);

    	//return new Response('Usuario:'. $user->getUsername(). ' con email: ' . $user->getEmail());

    	if(!$user){ //a pagina 404

    		$msgexc = $this->get('translator')->trans('User not found');

    		throw $this->createNotFoundException($msgexc);
    	}

    	$deleteForm=$this->createCustomForm($user->getId(), 'DELETE', 'dsg_user_delete');

    	return $this->render('DSGUserBundle:User:view.html.twig', array('user'=>$user, 'delete_form'=> $deleteForm->createView()));

    }

    /*private function createDeleteForm($user){


    	return $this->createFormBuilder()->setAction($this->generateUrl('dsg_user_delete', array('id'=> $user->getId())))
    										->setMethod('DELETE')
    										->getForm();
    }*/

    public function deleteAction(Request $request, $id){

    	$em=$this->getDoctrine()->getManager();
    	$user = $em->getRepository('DSGUserBundle:User')->find($id);

    	if(!$user){ //a pagina 404

    		$msgexc = $this->get('translator')->trans('User not found');

    		throw $this->createNotFoundException($msgexc);
    	}

    	$allUsers=$em->getRepository('DSGUserBundle:User')->findAll();
    	$countUsers = count($allUsers);

    	//$form = $this->createDeleteForm($user);
    	$form = $this->createCustomForm($user->getId(),'DELETE','dsg_user_delete');

    	$form->handleRequest($request);

    	if($form->isSubmitted() && $form->isValid() ){

    		if($request->isXMLHttpRequest()){ //todo lo que es de ajax

    			$res = $this->deleteUser($user->getRole(), $em, $user);

    			return new Response( json_encode(array('removed'=>$res['removed'], 'message'=> $res['message'], 'countUsers'=>$countUsers, )),200, array('Content-Type'=> 'application/json'));

    		}
    			/*$em->remove($user);
    			$em->flush();

    			$successMessage = $this->get('translator')->trans('The user has been deleted');
*/
    		$res = $this->deleteUser($user->getRole(), $em, $user);

    		//$this->addFlash('mensaje',$successMessage);
    		$this->addFlash($res['alert'],$res['message']);


    		return $this->redirectToRoute('dsg_user_index');


    	}




    }

    private function deleteUser($role, $em,$user){

    	if($role=='ROLE_USER'){
    		$em->remove($user);
    		$em->flush();

    		$message = $this->get('translator')->trans('The user has been deleted');

    		$remove=1;

    		$alert = 'mensaje';


    	}
    	elseif($role=='ROLE_ADMIN'){

    		$message = $this->get('translator')->trans('The user could not be deleted');
    		$remove=0;

    		$alert = 'error';

    	}

    	return array('removed'=>$remove, 'message' => $message, 'alert'=> $alert);
    }

    private function createCustomForm($id, $method, $route){

    	return $this->createFormBuilder()
    		->setAction($this->generateUrl($route, array('id'=> $id)))
    		->setMethod($method)
    		->getForm();


    }

    public function articlesAction($page){

    	return new Response("este es mi articulo ".$page);
    }
}
