<?php

namespace DSG\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response; 
use DSG\UserBundle\Entity\Task;
use DSG\UserBundle\Form\TaskType;
class TaskController extends Controller
{
	public function indexAction(Request $request){
		$em=$this->getDoctrine()->getManager();
		$dql="select t from DSGUserBundle:Task t order by t.id DESC";

		$tasks = $em->createQuery($dql);

		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate($tasks, $request->query->getInt('page',1), 3);

		return $this->render('DSGUserBundle:Task:index.html.twig', array('pagination'=> $pagination));
	}

	public function customAction(Request $request){
		$iduser= $this->get('security.token_storage')->getToken()->getUser()->getId();

		$em=$this->getDoctrine()->getManager();

		$dql="select t from DSGUserBundle:Task t join t.user u where u.id=:iduser order by t.id desc";

		$tasks = $em->createQuery($dql)->setParameter('iduser',$iduser);

		$paginator = $this->get('knp_paginator');

		$pagination = $paginator->paginate($tasks, 
			$request->query->getInt('page',1),
			3);

		$updateForm=$this->createCustomForm(':TASK_ID','PUT','dsg_task_process');

		return $this->render('DSGUserBundle:Task:custom.html.twig',array('pagination'=>$pagination, 'update_form' => $updateForm->createView()));

	}
	public function processAction($id, Request $request){
		$em = $this->getDoctrine()->getManager();

		$task = $em->getRepository('DSGUserBundle:Task')->find($id);

		if(!$task){
			throw $this->createNotFoundException('Task not found!!');
		}

		$form=$this->createCustomForm($task->getId(), 'PUT', 'dsg_task_process');
		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()){

			$successMessage = $this->get('translator')->trans('The task has been processed.');
            $warningMessage = $this->get('translator')->trans('The task has already been processed.');


			if($task->getStatus()==0)
			{
				$task->setStatus(1);
				$em->flush();
				if($request->isXMLHttpRequest())
				{
					return new Response(
						json_encode(array('processed'=>1, 'success' => $successMessage)),
						200,
						array('Content-Type'=> 'application/json')
						);
				}
			}
			else{

				if($request->isXMLHttpRequest())
				{
					return new Response(
						json_encode(array('processed'=>0, 'warning' => $warningMessage)),
						200,
						array('Content-Type'=> 'application/json')
						);
				}

			}
		}
	}
	public function addAction(){
		$task = new Task();
		$form = $this->createCreateForm($task);

		return $this->render('DSGUserBundle:Task:add.html.twig', array('form'=>$form->createView()));

	}
	private function createCreateForm(Task $entity){
		$form = $this->createForm(new TaskType(), $entity, array(
			'action' => $this->generateUrl('dsg_task_create'),
			'method' =>'POST'
			));
		return $form;
	}

	public function createAction(Request $request){
		$task = new Task();
		$form = $this->createCreateForm($task);
		$form->handleRequest($request);

		if($form->isValid()){
			$task->setStatus(0);
			$em=$this->getDoctrine()->getManager();
			$em->persist($task);
			$em->flush();

			$this->addFlash('mensaje', 'The task has been created');
			return $this->redirectToRoute('dsg_task_index');
		}
		return $this->render('DSGUserBundle:Task:add.html.twig', array('form'=>$form->createView()));
	}

	public function viewAction($id){
		$task = $this->getDoctrine()->getRepository('DSGUserBundle:Task')->find($id);

		if(!$task){
			throw $this->createNotFoundException('The task does not exist');
		}

		$deleteForm=$this->createCustomForm($task->getId(),'DELETE','dsg_task_delete');

		$user = $task->getUser();


		return $this->render('DSGUserBundle:Task:view.html.twig', array('task'=>$task, 'user'=>$user, 'delete_form'=>$deleteForm->createView()));

	}

	public function editAction($id){
		$em=$this->getDoctrine()->getManager();

		$task = $em->getRepository('DSGUserBundle:Task')->find($id);

		if(!$task){
			throw $this->createNotfoundException('task not found');
		}

		$form=$this->createEditForm($task);

		return $this->render('DSGUserBundle:Task:edit.html.twig', array('task' => $task, 'form'=> $form->createView()));

	}

	private function createEditForm(Task $entity){
		$form = $this->createForm(new TaskType(),$entity, array(
			'action'=>$this->generateUrl('dsg_task_update', array('id' => $entity->getId())) , 'method' => 'PUT'));

		return $form;
	}


	public function  updateAction($id, Request $request){

		$em = $this->getDoctrine()->getManager();

		$task = $em->getRepository('DSGUserBundle:Task')->find($id);

		if(!$task){
			throw $this->createNotfoundException('task not found');
		}

		$form=$this->createEditForm($task);

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid())
		{

			$task->setStatus(0);

			$em->flush();

			$this->addFlash('mensaje', 'Tha task has been modified');

			return $this->redirectToRoute('dsg_task_edit',array('id'=> $task->getId()));
		}

		return $this->render('DSGUserBundle:Task:edit.html.twig',array('task'=>$task,'form'=>$form->createView()));

	}

	public function deleteAction(Request $request, $id){
		$em=$this->getDoctrine()->getManager();

		$task = $em->getRepository('DSGUserBundle:Task')->find($id);

		if(!$task){
			throw $this->createNotfoundException('task not found');
		}

		$form = $this->createCustomForm($task->getId(),'DELETE','dsg_task_delete');

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()){
			$em->remove($task);
			$em->flush();

			$this->addFlash('mensaje','The task has been deleted');

			return $this->redirectToRoute('dsg_task_index');
		}


	}

	private function createCustomForm($id, $method, $route){

		return $this->createFormBuilder()
			->setAction($this->generateUrl($route,array('id' => $id )))
			->setMethod($method)
			->getForm();
	}
}
